# ANALYSE TRIS dont sort()

## Tri sort() de Python

Graphique obtenu avec la fonction `sort()` de Python

![Tri sort() ](Images/tri_sort.png)

## Tri insertion 

Graphique obtenu avec l'algo `insertion` 

![Tri insertion ](Images/tri_insert.png)

## Tri selection 

Graphique obtenu avec l'algo `selection`

![Tri selection ](Images/tri_select.png)

## Tri fusion

Graphique obtenu avec l'algo `fusion`
![Tri fusion ](Images/tri_fusion.png)

## Tri rapide

Graphique obtenu avec l'algo `rapide`

![Tri rapide ](Images/tri_rapide.png)

## Questions élèves

### Effectue le relevé dans un tableau du temps en s pour une taille de 100 pour chaque tri

### Ajoute une ligne dans le tableau et calcul le rapport entre les temps précédents et celui du tri `sort()`

### En déduire le tri le plus performant par rapport au tri `sort()`

### Evaluation :

Dans le repère suivant, associe chaque graphique à un des tris étudiés:
![Tous les tris ](Images/all.png)

